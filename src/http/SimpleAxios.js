import axios from 'axios';


axios.interceptors.request.use((config) => {
    const data = localStorage.getItem('loginInfo')
    if (data) {
        const { token } = JSON.parse(data)
        config.headers.Authorization = token
    }
    return config
})

export default axios;