import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/index.vue'
import userAdmin from "@/views/admin/userAdmin.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: HomeView,
    meta: { requireAuth: true}
  },
  {
    path: '/login',
    name:'login',
    component: () => import('../views/login/LoginView.vue'),
    meta:{requireAuth:true}
  },
  {
    path: '/mailbox',
    component: () => import('../views/login/MailboxPassword.vue')
  },
  {
    path:'/updatePassword',
    component: ()=>import('../views/login/UpdatePassword.vue')
  },
  {
    path:'/register',
    component: ()=>import('../views/login/RegisterUser.vue')
  },
  // {
  //   path:'/adminHome',
  //   component: ()=>import('../views/admin/HomeView.vue')// 暂定
  // },
  {
    path:'/adminLogin',
    component: ()=>import('../views/admin/HomeView.vue')
  },
  {
    path:'/test',
    component: ()=>import('../views/login/test.vue')
  },
  {
    path: '/admin:9999',
    component:userAdmin,
    children:[
        //文章管理
      //   {
      //   path:'/admin:9999/article',
      //     component:()=>import('../views/admin/article/Article.vue')
      // },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
const blacklist=['/']
const whiteList = ['/login',]
router.beforeEach((to,from,next)=>{
  const token = localStorage.getItem('loginInfo')
  if(token){
    if (whiteList.includes(to.path)) {
      next('/')//已登录
    } else {
      next()
    }
  } else {
    if (blacklist.includes(to.path)) {
      next()//未登录
    } else {
      next()
    }
  }
})

export default router
